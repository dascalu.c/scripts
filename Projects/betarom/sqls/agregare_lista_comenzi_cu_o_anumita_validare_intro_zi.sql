-- identifica comenzile ce au fost validate intr-o anumita zi
select LISTAGG(nrcomanda, ''',''') WITHIN GROUP (ORDER BY nrcomanda) AS csv_list from comanda cmd left join tiplivrare deliveryType on deliveryType.id=cmd.TIPLIVRARE_ID where
cmd.anulata=0 and cmd.markedfordeletion=0 and cmd.comandaferma=1
and cmd.driverphoneno is not null
and lower(deliveryType.label) like 'ruta%'
and VALIDATIONFLOW_ID in (
select fis.parent_id from flowinstancestep fis
left join flowdefinitionstep fds on fds.id=fis.definitionstep_id
left join characteristics chr on chr.id=fds.status_id
where fis.validat=1 and chr.internalcode='INCARCAT_IN_MASINA' and TRUNC(fis.dateupdated)=to_date('2024-09-26','yyyy-mm-dd')
and is_current=0);